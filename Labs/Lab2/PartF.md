[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)

### Part F - Fine-tune Startup Conditions
**Problem:** It turns out that there is a slight disconnect in state when the
web page has been reloaded after manipulating the LEDs from the buttons.

**Objective:** Because Javascript functions are by nature asynchronous, we can't
necessarily control the order of those functions firing.  However we need some
predictable start up sequencing when the HTML page loads and starts running the
script section of the code.  

#### Step F1: Build a Javascript function sequence in CodePen
We will use the jQuery Deferred execution model with 'resolve' and 'promise'
methods to synchronize the execution.
```javascript
function firstFunction() {
  var d = $.Deferred();
  // some very time consuming asynchronous code...
  setTimeout(function() {
    console.log('1');
    d.resolve();
  }, 1000);
  return d.promise();
}

function thirdFunction() {
  var d = $.Deferred();
  setTimeout(function() {
    console.log('3');
    d.resolve();
  }, 1000);
  return d.promise();
}

function secondFunction() {
  var d = $.Deferred();
  setTimeout(function() {
    console.log('2');
    d.resolve();
  }, 1000);
  return d.promise();
}

function fourthFunction() {
  var d = $.Deferred();
  // last function, not executed until the other 3 are done.
  setTimeout(function() {
    console.log('4');
    d.resolve();
  }, 1000);
  return d.promise();
}

// sequence through the synchronous functions using '.then'
firstFunction().then(thirdFunction).then(secondFunction).then(fourthFunction);
```

#### Step F2: Code a similar startup sequence for LED state variables
```javascript
// Let's read the current LED state
function initial_conditions() {
  var d = $.Deferred();

  setTimeout(function() {
    $.get('/leds/1',function(data){
      led1 = $.trim(data.split(':')[1]);
    });

    $.get('/leds/2',function(data){
      led2 = $.trim(data.split(':')[1]);
    });

    $.get('/leds/3',function(data){
      led3 = $.trim(data.split(':')[1]);
    });

    // console.log("Got my data now!");
    d.resolve();
  }, 1);
  return d.done();
}

// Let's initialize our LED vars to the current LED state "ON"/"OFF"
function led_status() {
  var d = $.Deferred();

  setTimeout(function() {
    if (led1 === '0') {led1 =  "OFF"} else {led1 =  "ON"}
    if (led2 === '0') {led2 =  "OFF"} else {led2 =  "ON"}
    if (led3 === '0') {led3 =  "OFF"} else {led3 =  "ON"}
    d.resolve();

    console.log("RED:",led1);
    console.log("GRN:",led2);
    console.log("BLU:",led3);
  }, 200);
  return d.promise();
}

// make sure to intialize synchronously (10ms back to back)
initial_conditions().then(led_status);
```
**Great!** We now have a fully functional sensor/actuator monitoring and control
dashboard webapp.  In our upcoming lab we will make this a more dynamic dashboard
that will automatically update indications using "Server Sent Events".


[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)
