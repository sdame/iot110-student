[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)

### Part B -  Simple Flask API Webserver ###
**Objective:** We will be using HTTP GET and POST APIs as our basic method of
interfacing between our Webapp and the Python sensor and actuator driver code.  
The following is just an exercise to focus on how APIs work and how to test them
from the CLI (bash shell).

#### Step B1: import Flask and request support
Open a new file called ```api_server.py``` and enter the following sections:
Import the same structure as ```hello.py``` but provide support for parsing
requests.  Get the hostname as well and instantiate the Flash app object.
Place the app.run at the bottom of the file.  We will add each API and perform
a CURL test on each as we add them.
```python
from flask import Flask, request
import socket

## Get my machine hostname
if socket.gethostname().find('.')>=0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)


... API routes to be added below

## Run the website and make sure to make
##  it externally visible with 0.0.0.0:5000 (default)
if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
```

#### Step B2: Add and Test Routes/API types
Add API routes for DEFAULT and a basic GET REQUEST.
```python
#DEFAULT Route
# curl http://0.0.0.0:5000/
@app.route("/")
def hello():
    return "Hello API Server : Default Request from (hostname) : " + hostname + "!\n"

#GET REQUEST
# curl http://0.0.0.0:5000/getHello
@app.route('/getHello')
def getRequestHello():
    return "Hello API Server : GET Request!\n"
```

Test these first two APIs by CURLing their hostname:5000
```sh
host$ curl iot8e3c.home:5000
=> Hello API Server : Default Request from (hostname) : iot8e3c.home!

host$ curl iot8e3c.home:5000/getHello
=> Hello API Server : GET Request!
```

Now, let's add a HTTP POST API request. **Note:** you can just add in the new
and save and the Flask framework will automatically restart for you after
detecting a file change.
```python
#POST REQUEST
# curl --data 'mykey=FOOBAR' http://0.0.0.0:5000/createHello
# echo 'mykey={"name":"Gene Cernan","age":"82"}' | curl -d @- http://0.0.0.0:5000/createHello
@app.route('/createHello', methods = ['POST'])
def postRequestHello():
    mydata = request.data
    print "Data:" + mydata
    assert request.path == '/createHello'
    assert request.method == 'POST'
    data = str(request.form['mykey'])
    # import pdb; pdb.set_trace()
    return "Hello API Server : You sent a "+ request.method + \
            " message on route path " + request.path + \
            " \n\tData:" +  data + "\n"
```

Test the POST API by CURLing with a slightly different way of passing data. We
will explore a couple of methods:
```sh
host$ curl --data 'mykey=FOOBAR' iot8e3c:5000/createHello
=> Hello API Server : You sent a POST message on route path /createHello
	 Data:FOOBAR

host$ echo 'mykey={"name":"Gene Cernan","age":"82"}' | curl -d @- iot8e3c:5000/createHello
=> Hello API Server : You sent a POST message on route path /createHello
	 Data:{"name":"Gene Cernan","age":"82"}
```
**GREAT!** Now you are empowered to create all manner of GET and POST API calls.

[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)
