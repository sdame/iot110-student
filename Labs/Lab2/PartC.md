[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)

### Part C -  GPIO Webserver ###
**Objective:** Now its time to flesh out our main web server application ```main.py```
that will serve as the main interface between our GPIO python driver and the HTML
webpage ```index.html```.

#### Step C1: import Flask and request support and main execution (bottom)
Let's start by importing our Flask framework as well as our GPIO driver module
and instantiating objects for each:
```python
from gpio import PiGpio
from flask import *

app = Flask(__name__)
pi_gpio = PiGpio()

... add APIs <here>

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
```

#### Step C2: Default Route
Next we need to add the default route.  In other words, this is the route that
will be executed whenever the main webpage is rendered on port 5000.  Note that
this default webpage load also contains code to read the current state of the
hardware and place in some python local variables.  These variables are then
passed by some new variable namespace that will be passed to
a *template* ```index.html``` file (located in a templates directory off the
root directory).  As we will see shortly these variables are accessible to the
HTML document in a special embedded scripting syntax  {{ python code }} supported
by the Flask framework.
```python
@app.route("/")
def index():
    # create an instance of my pi gpio object class.
    pi_gpio = PiGpio()
    switch_state = pi_gpio.read_switch()
    led1_state = pi_gpio.get_led(1)
    led2_state = pi_gpio.get_led(2)
    led3_state = pi_gpio.get_led(3)
    return render_template('index.html', switch=switch_state,
                                led1=led1_state,
                                led2=led2_state,
                                led3=led3_state)
```

#### Step C3: Add all of our GPIO API Routes
```python
# ============================== API Routes ===================================
# ============================ GET: /leds/<state> =============================
# read the LED status by GET method from curl for example
# curl http://iot8e3c:5000/led/1
# curl http://iot8e3c:5000/led/2
# -----------------------------------------------------------------------------
@app.route("/leds/<int:led_state>", methods=['GET'])
def leds(led_state):
  return "LED State:" + str(pi_gpio.get_led(led_state)) + "\n"


# =============================== GET: /sw ====================================
# read the switch input by GET method from curl for example
# curl http://iot8e3c:5000/sw
# -----------------------------------------------------------------------------
@app.route("/sw", methods=['GET'])
def sw():
  return "Switch State:" + str(pi_gpio.read_switch()) + "!\n"

# ======================= POST: /ledcmd/<data> =========================
# set the LED state by POST method from curl. For example:
# curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
# -----------------------------------------------------------------------------
@app.route("/ledcmd", methods=['POST'])
def ledcommand():
    cmd_data = request.data
    print "LED Command:" + cmd_data
    led = int(str(request.form['led']))
    state = str(request.form['state'])
    if(state == 'OFF'):
        pi_gpio.set_led(led,False)
    elif (state == 'ON'):
        pi_gpio.set_led(led,True)
    else:
        return "Argument Error"

    return "Led State Command:" + state + " for LED number:"+ str(led) + "\n"
    # -----------------------------------------------------------------------------
# ============================== API Routes ===================================
```

#### Step C4: Test all of our API routes using CURL as indicated in comments.
```sh
# SW not pressed
host$ curl http://iot8e3c:5000/sw
=> Switch State:0!

# SW pressed
host$ curl http://iot8e3c:5000/sw
=> Switch State:1!

# LED GET requests
host$ curl http://iot8e3c:5000/leds/1
=> LED State:1

host$ curl http://iot8e3c:5000/leds/2
=> LED State:1

host$ curl http://iot8e3c:5000/leds/3
=> LED State:1

# Change the LED states to ON
host$ curl --data 'led=1&state=ON' http://iot8e3c:5000/ledcmd
=> Led State Command:ON for LED number:1

host$ curl --data 'led=2&state=ON' http://iot8e3c:5000/ledcmd
=> Led State Command:ON for LED number:2

host$ curl --data 'led=3&state=ON' http://iot8e3c:5000/ledcmd
=> Led State Command:ON for LED number:3

# Change the LED states to OFF
host$ curl --data 'led=1&state=OFF' http://iot8e3c:5000/ledcmd
=> Led State Command:ON for LED number:1

host$ curl --data 'led=2&state=OFF' http://iot8e3c:5000/ledcmd
=> Led State Command:ON for LED number:2

host$ curl --data 'led=3&state=OFF' http://iot8e3c:5000/ledcmd
=> Led State Command:ON for LED number:3
```

[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)
