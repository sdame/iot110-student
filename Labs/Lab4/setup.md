[IOT STUDENT HOME](https://bitbucket.org/sdame/iot110-student/src/f33a042d80f3?at=master)

## Setting up Lab4

### Objectives
For this lab we will continue building on our IoT suite by adding our first
active sensor device.  We will be adding the *Bosch BMP280* Pressure/Temperature
sensor and interfacing to it using the I2C serial protocol standardized by
Philips (NXP) Semiconductor.  

The various steps of this lab are summarized as:

- [PART A](https://bitbucket.org/sdame/iot110-student/src/473ebf6aa4a5454f7f936b382337ae41d471618a/Labs/Lab4/PartA.md?at=master&fileviewer=file-view-default) - Enable I2C Bus and Test Connection to BMP280
- [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartB.md) - Create BMP280 Python Driver
- [PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/PartC.md) - Create BMP280 Python Driver Unit Test

[IOT STUDENT HOME](https://bitbucket.org/sdame/iot110-student/src/f33a042d80f3?at=master)

